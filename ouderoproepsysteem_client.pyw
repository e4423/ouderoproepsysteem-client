#!/usr/bin/python3

import requests
import os
import json
import psutil
import easygui
import time
import datetime
import configparser
import logging

version = 'v2.1'
versiondate = '04-05-2022'

# Parse configuration file:
config = configparser.ConfigParser()
try:
    config.read('ouderoproepsysteem_config.txt')
except Exception:
    easygui.msgbox("\nHet configuratiebestand kan niet gevonden worden!", title="Ouderoproepsysteem start niet!")


def is_PP_running():
    """
    Checks if ProPresenter is running on this machine. If so returns TCPport of the running process
    """
    
    PPpid = 0

    for proc in psutil.process_iter():
        if "ProPresenter Helper - Network.exe".lower() in proc.name().lower():
            PPpid = proc.pid
            PPport = 0
            for connection in psutil.net_connections():
                if connection.pid == PPpid and connection.status == "LISTEN":
                    PPport = connection.laddr.port
            logging.info('"ProPresenter Helper - Network.exe" is running with PID {} on TCP PORT {}'.format(PPpid, PPport))
            return PPport
 
    if PPpid == 0:
        logging.warning('"ProPresenter Helper - Network.exe" is NOT running')
        return False
    else:
        return PPport

def flask_poller(flask_hostname, flask_username, flask_password):
    """
    Checks if new messages are available on flask server. Returns a dict with alle messages e.g. {'105': '23/03/2022 20:04:57'}
    """
    
    url = "{}/api/v1/propresenter".format(flask_hostname)
    req = requests.get(url, auth=(flask_username, flask_password))
    if req.status_code == 200:
        logging.info('Successfull fetch from server. {}'.format(req))
    else:
        logging.warning('Failed to fetch from server. {}'.format(req))
    
    return(json.loads(req.text).get('Oproepen'))

def flask_updater(flask_hostname, flask_username, flask_password, oproepen):
    """
    Removes Oproepen from flask server when handled by this script
    """
    url = "{}/api/v1/opgeroepen".format(flask_hostname)

    for oproep in oproepen:
        data = {"Oproepen": {oproep : "To be removed"}}
        req = requests.post(url, json=data, auth=(flask_username, flask_password))
        if req.status_code == 200:
            logging.info('Successfully removed {} from server. {}'.format(oproep,req))
        else:
            logging.warning('Failed to remove {} from server. {}'.format(oproep,req))
        print(req.text)
        
def send_to_PP(PPport, message):
    """
    Send a message to Propresenter on webform. This function retreives a TableID and a FieldID of the webform.
    The message schould be a string.
    """

    global tableID
    global fieldsID
    
    req = requests.get('http://127.0.0.1:{}/html/data/messages'.format(PPport))
    jsn = json.loads(req.text)
    tableID = jsn[0]['id']
    fieldsID = jsn[0]['fields'][0]['id']
    
    if req.status_code == 200:
        logging.info('Successfully scraped tableID: {} and fieldsID: from Propresenter. {}'.format(tableID, fieldsID, req))
    else:
        logging.warning('Failed to scrape tableID and fieldID form Propresenter. {}'.format(req))
    
    
    payload = '{"id":"'+ tableID +'","fields":[{"id":"'+ fieldsID +'","value":"' + str(message) + '"}]}' #needs to be a string, otherwise ProPresenter doesn't accept it.
    headers = {'Content-type':'application/json'}
    r = requests.post('http://127.0.0.1:{}/html/data/request'.format(PPport), headers=headers, data=payload)
    print(r.text)
    if req.status_code == 200:
        logging.info('Successfully posted message to Propresenter. {}'.format(req))
    else:
        logging.warning('Failed to post message to Propresenter. {}'.format(req))                       

def main():
         
    #Create logfile:
    log_directory = config.get('SETTINGS', 'log_directory').rstrip("\\")
    if not os.path.isdir(log_directory):
        os.mkdir(log_directory)
    logging.basicConfig(filename='{}\\{}.log'.format(log_directory, datetime.datetime.now().strftime("%Y%m%d")), format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)

    logging.info('Starting Ouderoproepsysteem Client - {}'.format(version))
    logging.info('Sleeping for 30s before first run....')
    time.sleep(30)
 
    while True:
        oproepen = {}
        try:
            logging.info('Normal fetch')
            oproepen = flask_poller(config.get('SETTINGS', 'flask_hostname'), config.get('SETTINGS','flask_username'), config.get('SETTINGS','flask_password'))
        except Exception:
            logging.warning('Failed fetch. Speeding things up to recover for the next 5 minutes. Hold tight!')
            count = 0
            while True:
              time.sleep(10)
              try:
                  logging.warning('Recovery fetch: {}'.format(count + 1))
                  oproepen = flask_poller(config.get('SETTINGS', 'flask_hostname'), config.get('SETTINGS','flask_username'), config.get('SETTINGS','flask_password'))
                  break
              except Exception:
                   count += 1
                   if count == 30:
                      logging.error('Failed to recover! Popup shown to user...')
                      easygui.msgbox("Oproepen al 5 minuten niet mogelijk... Server voor ouderoproepsysteem onbereikbaar!\n \nKijk op {} voor aanvullende informatie.\n\nAls dit probleem blijft bestaan zal een alternatieve manier voor het ontvangen van kindnummers bedacht moeten worden.".format(config.get('SETTINGS', 'flask_hostname')), title="Server voor ouderoproepsysteem onbereikbaar!")
                      break
                   else:
                       pass
        if len(oproepen) > 0:
            message = ", ".join(list(oproepen))
            logging.info('Nieuwe oproep voor {}'.format(message))
            if not is_PP_running():
                logging.warning('Showing popup to user for new message')
                easygui.msgbox("\nOproep voor: {}\n\nVoer dit nummer handmatig in in ProPresenter.".format(message), title="ProPresenter Webservice functioneert niet!")
                try:
                    flask_updater(config.get('SETTINGS', 'flask_hostname'), config.get('SETTINGS','flask_username'), config.get('SETTINGS','flask_password'), oproepen)
                except Exception:
                    pass
            else:
                send_to_PP(is_PP_running(), message)
                try:
                    flask_updater(config.get('SETTINGS', 'flask_hostname'), config.get('SETTINGS','flask_username'), config.get('SETTINGS','flask_password'), oproepen)
                except Exception:
                    pass
        logging.info('Taking a nap for 30s before doing it all over again...')
        time.sleep(30)

if __name__ == '__main__':
    main()

